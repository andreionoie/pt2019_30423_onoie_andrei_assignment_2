package QueueSimStrategy;

import QueueSimLogic.Server;
import QueueSimLogic.Task;

import java.util.List;

public class ConcreteStrategyQueue implements Strategy {
    // go for queue with smallest number of tasks ahead
    public void addTask(List<Server> servers, Task t) {
        if (servers.isEmpty())
            return;

        int minTasks = Integer.MAX_VALUE;
        Server target = null;

        for (Server s : servers)
            if (s.getNumberOfTasks() < minTasks) {
                minTasks = s.getNumberOfTasks();
                target = s;
            }

        target.addTask(t);
        System.out.println("Dispatched task " + t + " to server #" + (target.getId() + 1) + ".");
    }
}
