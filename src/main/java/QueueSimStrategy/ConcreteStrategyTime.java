package QueueSimStrategy;

import QueueSimLogic.Server;
import QueueSimLogic.Task;

import java.util.List;

public class ConcreteStrategyTime implements Strategy {
    // go for queue with shortest waiting timeKeeper
    public void addTask(List<Server> servers, Task t) {
        if (servers.isEmpty())
            return;

        int minTime = Integer.MAX_VALUE;
        Server target = null;

        for (Server s : servers) {
            if (s.getTotalServiceTime() < minTime) {
                minTime = s.getTotalServiceTime();
                target = s;
            }
        }

        target.addTask(t);
        System.out.println("Dispatched task " + t + " to server #" + (target.getId() + 1) + ".");
    }
}
