package QueueSimStrategy;

import QueueSimLogic.Server;
import QueueSimLogic.Task;

import java.util.List;

public interface Strategy {
    public void addTask(List<Server> servers, Task t);
}
