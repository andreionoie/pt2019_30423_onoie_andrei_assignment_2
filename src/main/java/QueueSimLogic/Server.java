package QueueSimLogic;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
    private BlockingQueue<Task> tasks;
    public static AtomicInteger timeKeeper = new AtomicInteger(0);
    private int id;
    private int sleepPeriod;

    private boolean isRunning = true;

    public Server(int maxSize, int id, int sleepPeriod) {
        tasks = new ArrayBlockingQueue<Task>(maxSize);
        this.id = id;
        this.sleepPeriod = sleepPeriod;
    }

    public void addTask(Task t) {
        tasks.add(t);
        //waitingPeriod.getAndAdd(t.getProcessingTime());
    }

    public void run() {
        while (isRunning) {
           // take next task from queue
           // stop the thread for a timeKeeper equal with procTime
           try {
               Thread.sleep(sleepPeriod);

               if (tasks.isEmpty())
                   continue;

               tasks.peek().decServiceTime();


               if (tasks.peek() != null) {
                   System.out.println("QueueSimLogic.Server #" + (this.id + 1) + " has processed task " + tasks.peek() + "");

                   if (tasks.peek().isFinished()) {
                       Task t = tasks.take();
                       t.setFinishingTime(Server.timeKeeper.get());
                       System.out.println("----!!---- QueueSimLogic.Task #" + t.getId() + " exiting, total waiting time=" + t.getTotalWaitingTime());
                   }
               }

           } catch (Exception e) {
               e.printStackTrace();
               isRunning = false;
           }
        }
    }

    public Task[] getTasks() {
        Task[] out = new Task[tasks.size()];
        int i=0;

        for (Task t : tasks)
            out[i++] = t;

        return out;
    }

    public int getTotalServiceTime() {
        int total = 0;
        for (Task t : tasks)
            total += t.getProcessingTime();

        return total;
    }

    public int getNumberOfTasks() {
        return tasks.size();
    }

    public int getId() {
        return id;
    }

    public void kill() {
        isRunning = false;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder("Queue ID=" + id + ", status: ");

        for (Task t : tasks) {
            sb.append(" " + t.toString());
        }

        return sb.toString();
    }
}
