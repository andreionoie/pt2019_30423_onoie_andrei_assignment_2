package QueueSimLogic;

import QueueSimStrategy.SelectionPolicy;
import QueueSimUI.SimulationDialog;

import java.util.*;

public class SimulationManager implements Runnable {
    private int timeLimit;
    private int maxProcessingTime ;
    private int minProcessingTime;
    private int numberOfServers;
    private int numberOfClients;
    private SelectionPolicy sp;
    private int sleepPeriod;

    private int peakHour = 0;
    // entity responsible with queue management and client distribution
    private Scheduler scheduler;
    // jframe
    private SimulationDialog frame;
    // task pool
    private List<Task> generatedTasks;

    public SimulationManager(int timeLimit, int maxProcessingTime, int minProcessingTime, int numberOfServers, int numberOfClients, SelectionPolicy sp, int sleepPeriod) {
        this.timeLimit = timeLimit;
        this.maxProcessingTime = maxProcessingTime;
        this.minProcessingTime = minProcessingTime;
        this.numberOfServers = numberOfServers;
        this.numberOfClients = numberOfClients;
        this.sp = sp;
        this.sleepPeriod = sleepPeriod;
        generatedTasks = new ArrayList<Task>();
        scheduler = new Scheduler(this.numberOfServers, this.numberOfClients / this.numberOfServers + 1, this.sleepPeriod);
        scheduler.changeStrategy(this.sp);
        frame = new SimulationDialog();

        Server.timeKeeper.set(0);
        generateRandomTasks();
    }

    private void generateRandomTasks() {
        generatedTasks.clear();
        Random r = new Random();

        for (int i=0; i < numberOfClients; i++) {
            int serviceTime = minProcessingTime + r.nextInt(maxProcessingTime - minProcessingTime + 1);
            int waitingTime = 1 + r.nextInt(timeLimit - 1);
            generatedTasks.add(new Task(waitingTime, serviceTime, i));
            System.out.println("Generated task #" + i + " w/ service time " + serviceTime + ", waiting time " + waitingTime + ".");
        }

        // sort ascending by arrival timeKeeper
        Collections.sort(generatedTasks, new Comparator<Task>() {
            public int compare(Task o1, Task o2) {
                return o1.getArrivalTime() - o2.getArrivalTime();
            }
        });
    }

    public void run() {
        int currentTime = 0;
        int maxTasks = 0;
        int[] emptyQ = {0, 0 ,0};
        String avgWaitingTime = Math.round(getAvgWaitingTime()*100.0)/100.0 + "";
        String avgServiceTime = Math.round(getAvgServiceTime()*100.0)/100.0 + "";

        Iterator itr = generatedTasks.iterator();
        Task t = (Task) itr.next();

        List<Task> toBeScheduled = new ArrayList<Task>();
        while (currentTime < timeLimit) {
            if (! frame.isVisible())
                break;

            // skip to first task with arrivalTime == currentTime
            while (itr.hasNext() && t.getArrivalTime() < currentTime)
                t = (Task) itr.next();
            // add all tasks with arrivalTime == currentTime to a list
             while (t.getArrivalTime() == currentTime) {
                toBeScheduled.add(t);
                if (itr.hasNext())
                    t = (Task) itr.next();
                else
                    break;
            }

            System.out.print("Dispatching tasks: ");
            for (Task tk : toBeScheduled)
                System.out.print("#" + tk.getId() + " ");
            System.out.println();

            // send tasks to queues using QueueSimLogic.Scheduler
            for (Task tk : toBeScheduled) {
                scheduler.dispatchTask(tk);
            }


            // update UI frame

            System.out.println("Tick no. " + currentTime + " has passed.");

            // get peak hour
            if (maxTasks < getNbOfCurrentTasks()) {
                maxTasks = getNbOfCurrentTasks();
                peakHour = currentTime;
            }

            // get empty q timeKeeper
            for (int i=0; i < 3; i++)
                if (scheduler.getServers().get(i).getNumberOfTasks() == 0)
                    emptyQ[i]++;

            // update frame info
            avgWaitingTime = Math.round(getAvgWaitingTime()*100.0)/100.0 + "";
            frame.displayData(getAllTasks());
            frame.updateLabels(peakHour, avgWaitingTime, avgServiceTime, emptyQ[0], emptyQ[1], emptyQ[2], currentTime);


            toBeScheduled.clear();

            try {
                Thread.sleep(sleepPeriod);
                currentTime++;
                Server.timeKeeper.incrementAndGet();
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }
        }

        for (Server s : scheduler.getServers())
            s.kill();

        System.out.println("Peak hour: " + peakHour + ", having had " + maxTasks + " total running tasks.");
    }

    private int getNbOfCurrentTasks() {
        int count = 0;
        for (Server s : scheduler.getServers())
            count += s.getNumberOfTasks();

        return count;
    }

    private double getAvgServiceTime() {
        double sum=0;

        for (Task t : generatedTasks)
            sum += t.getProcessingTime();


        return sum / generatedTasks.size();
    }

    private double getAvgWaitingTime() {

        double sum=0;
        int finishedTasksCount = 0;

        for (Task t : generatedTasks)
            if ((t.getTotalWaitingTime() > 0)) {
                sum += t.getTotalWaitingTime();
                finishedTasksCount++;
            }

        if (finishedTasksCount == 0)
            return 0;

        return sum / finishedTasksCount;
    }

    private Task[][] getAllTasks() {
        Task[][] tasks = new Task[scheduler.getServers().size()][];
        int i=0;
        for (Server s : scheduler.getServers()) {
            tasks[i] = s.getTasks();
            i++;
        }

        return tasks;
    }
}
