package QueueSimLogic;

import QueueSimStrategy.ConcreteStrategyQueue;
import QueueSimStrategy.ConcreteStrategyTime;
import QueueSimStrategy.SelectionPolicy;
import QueueSimStrategy.Strategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Scheduler {
    private List<Server> servers;
    private Map<Server, Thread> serverThreadMap;
    private int maxNoServers;
    private int maxTasksPerServer;
    private Strategy strategy;
    private int sleepPeriod;

    public Scheduler(int maxNoServers, int maxTasksPerServer, int sleepPeriod) {
        this.maxNoServers = maxNoServers;
        this.maxTasksPerServer = maxTasksPerServer;
        this.sleepPeriod = sleepPeriod;

        this.servers = new ArrayList<Server>();
        this.serverThreadMap = new HashMap<Server, Thread>();

        for (int i=0; i < maxNoServers; i++) {
            Server s = new Server(maxTasksPerServer, i, this.sleepPeriod);
            servers.add(s);

            Thread t = new Thread(s);
            t.start();

            serverThreadMap.put(s, t);
        }
    }


    public void changeStrategy(SelectionPolicy sp) {
        if (sp == SelectionPolicy.SHORTEST_QUEUE)
            strategy = new ConcreteStrategyQueue();

        if (sp == SelectionPolicy.SHORTEST_TIME)
            strategy = new ConcreteStrategyTime();
    }

    public void dispatchTask(Task t) {
        strategy.addTask(servers, t);
    }


    public List<Server> getServers() {
        return servers;
    }
}
