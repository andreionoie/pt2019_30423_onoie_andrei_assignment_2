package QueueSimLogic;

import java.util.concurrent.Delayed;

// https://www.baeldung.com/java-delay-queue

public class Task {
    private int arrivalTime;
    private int processingTime;
    private int id;
    private int totalWaitingTime;

    public Task(int waitingTime, int serviceTime, int id) {
        this.arrivalTime = waitingTime;
        this.processingTime = serviceTime;
        this.id = id;
        this.totalWaitingTime = -1;
    }

    public void decServiceTime() {
        processingTime--;
    }

    public boolean isFinished() {
        return processingTime <= 0;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getProcessingTime() {
        return processingTime;
    }

    public int getId() {
        return id;
    }

    public int getTotalWaitingTime() {
        return totalWaitingTime;
    }

    public void setFinishingTime(int finishingTime) {
        totalWaitingTime = finishingTime - arrivalTime;
    }

    @Override
    public String toString() {
        return "{ID=" + id + "}." + "{P=" + processingTime + "}.{A=" + arrivalTime + "}";
    }

}
