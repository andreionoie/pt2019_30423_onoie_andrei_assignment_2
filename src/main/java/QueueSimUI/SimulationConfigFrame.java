package QueueSimUI;

import QueueSimLogic.SimulationManager;
import QueueSimStrategy.SelectionPolicy;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimulationConfigFrame extends JFrame {
    private final int WIDTH=600, HEIGHT=350;
    private JSpinner simSpeed, timeLimitSpinner, nbOfQueues, nbOfClients, minProcTime, maxProcTime;
    private JRadioButton r1, r2;
    private JButton beginButton;

    public SimulationConfigFrame() {
        addComponents();

        setTitle("Queue Multithreading Simulation Configuration");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void addComponents() {
        setLayout(new GridLayout(4, 3));
        simSpeed = new JSpinner(new SpinnerNumberModel(2000, 10, 5000, 100));
        timeLimitSpinner = new JSpinner(new SpinnerNumberModel(100, 1, 9999, 5));
        nbOfQueues = new JSpinner(new SpinnerNumberModel(3, 1, 6, 1));
        nbOfClients = new JSpinner(new SpinnerNumberModel(100, 1, 500, 5));
        minProcTime = new JSpinner(new SpinnerNumberModel(2, 1, 100, 1));
        maxProcTime = new JSpinner(new SpinnerNumberModel(10, 1, 100, 1));


        r1 = new JRadioButton("Shortest Time");
        r1.setSelected(true);
        r2 = new JRadioButton("Shortest Queue");
        ButtonGroup selectionPolicy = new ButtonGroup();
        selectionPolicy.add(r1);
        selectionPolicy.add(r2);

        beginButton = new JButton("Begin simulation");
        beginButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int sleepPeriod = (Integer) simSpeed.getValue();
                int timeLimit = (Integer) timeLimitSpinner.getValue();
                int maxProcessingTime = (Integer) maxProcTime.getValue();
                int minProcessingTime = (Integer) minProcTime.getValue();
                int numberOfServers = (Integer) nbOfQueues.getValue();
                int numberOfClients = (Integer) nbOfClients.getValue();
                SelectionPolicy sp;

                if (r1.isSelected())
                    sp = SelectionPolicy.SHORTEST_TIME;
                else
                    sp = SelectionPolicy.SHORTEST_QUEUE;

                Thread t = new Thread(new SimulationManager(timeLimit,
                        maxProcessingTime, minProcessingTime, numberOfServers,
                        numberOfClients, sp, sleepPeriod));
                t.start();
            }
        });


        JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p1.add(new JLabel("Simulation speed (ns):"));
        p1.add(simSpeed);

        JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p2.add(new JLabel("Time limit:"));
        p2.add(timeLimitSpinner);

        JPanel p3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p3.add(new JLabel("Selection policy:"));
        p3.add(r1);
        p3.add(r2);

        JPanel p4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p4.add(new JLabel("Number of queues:"));
        p4.add(nbOfQueues);

        JPanel p5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p5.add(new JLabel("Number of clients:"));
        p5.add(nbOfClients);

        JPanel p6 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p6.add(new JLabel("Minimum processing time:"));
        p6.add(minProcTime);

        JPanel p7 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p7.add(new JLabel("Maximum processing time:"));
        p7.add(maxProcTime);


        add(p1);
        add(p2);
        add(p3);

        add(p4);
        add(p5);
        add(new JLabel());

        add(p6);
        add(p7);
        add(new JLabel());

        add(new JLabel());
        add(beginButton);
        add(new JLabel());
    }
}
