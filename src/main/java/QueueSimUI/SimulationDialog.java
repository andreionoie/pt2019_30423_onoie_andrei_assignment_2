package QueueSimUI;

import QueueSimLogic.Task;

import javax.swing.*;
import java.awt.*;
import java.io.PrintStream;
import java.util.ArrayList;

public class SimulationDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private JPanel settingsPanel, serversPanel;
    private JLabel  peakHourLabel,
                    avgWaitingTimeLabel,
                    avgServiceTimeLabel,
                    emptyQ1Label,
                    emptyQ2Label,
                    emptyQ3Label,
                    currentTimeLabel;

    private JTextArea logTextArea;
    private final int WIDTH=600, HEIGHT=600;

    public SimulationDialog() {
        addComponents();

        setTitle("Queue Multithreading Simulation");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void displayData(Task[][] tasks) {
        serversPanel.removeAll();

        String[][] tasksText = new String[tasks.length][];
        JScrollPane[] panes = new JScrollPane[tasks.length];

        for (int i=0; i < tasks.length; i++) {
            tasksText[i] = new String[tasks[i].length + 1];
            tasksText[i][0] = "Queue #" + (i+1);
            for (int j = 1; j <= tasks[i].length; j++)
                tasksText[i][j] = tasks[i][j-1].toString();

            JList tmpList = new JList(tasksText[i]);
            tmpList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tmpList.setLayoutOrientation(JList.VERTICAL);
            tmpList.setVisibleRowCount(15);

            panes[i] = new JScrollPane(tmpList);
        }

        for (int i=0; i < tasks.length; i++) {
            //System.out.println("Adding panel " + i + " " + panes[i]);
            serversPanel.add(panes[i]);
        }

        serversPanel.revalidate();
        serversPanel.repaint();
        serversPanel.updateUI();

    }

    public void updateLabels(int peakHour, String avgWaitingTime, String avgServiceTime, int emptyQ1, int emptyQ2, int emptyQ3, int currentTime) {
        peakHourLabel.setText("Peak hour: " + peakHour);
        avgWaitingTimeLabel.setText("Average waiting time: " + avgWaitingTime);
        avgServiceTimeLabel.setText("Average service time: " + avgServiceTime);
        emptyQ1Label.setText("Empty queue time for Q1: " + emptyQ1);
        emptyQ2Label.setText("Empty queue time for Q2: " + emptyQ2);
        emptyQ3Label.setText("Empty queue time for Q3: " + emptyQ3);
        currentTimeLabel.setText("Simulation time: " + currentTime);
    }

    public void addComponents() {
        settingsPanel = new JPanel(new GridLayout(5, 3));

        peakHourLabel = new JLabel("Peak hour: -");
        avgWaitingTimeLabel = new JLabel("Average waiting time: -");
        avgServiceTimeLabel = new JLabel("Average service time: -");
        emptyQ1Label = new JLabel("Empty queue time for Q1: -");
        emptyQ2Label = new JLabel("Empty queue time for Q2: -");
        emptyQ3Label = new JLabel("Empty queue time for Q3: -");
        currentTimeLabel = new JLabel("Simulation time: -");

        // row 1
        settingsPanel.add(peakHourLabel);
        settingsPanel.add(avgWaitingTimeLabel);
        settingsPanel.add(avgServiceTimeLabel);
        // row 2
        settingsPanel.add(emptyQ1Label);
        settingsPanel.add(emptyQ2Label);
        settingsPanel.add(emptyQ3Label);
        // row 3
        settingsPanel.add(new JSeparator());
        settingsPanel.add(new JSeparator());
        settingsPanel.add(new JSeparator());
        // row 4
        settingsPanel.add(new JLabel());
        currentTimeLabel.setHorizontalAlignment(JLabel.CENTER);
        settingsPanel.add(currentTimeLabel);
        settingsPanel.add(new JLabel());
        // row 5
        settingsPanel.add(new JSeparator());
        settingsPanel.add(new JSeparator());
        settingsPanel.add(new JSeparator());

        add(settingsPanel, BorderLayout.NORTH);

        serversPanel = new JPanel(new GridLayout(1, 0));
        add(serversPanel, BorderLayout.CENTER);

        logTextArea = new JTextArea(10, 200);
        logTextArea.setEditable(false);
        logTextArea.setBackground(Color.LIGHT_GRAY);
        logTextArea.setForeground(Color.BLACK);
        logTextArea.setFont(logTextArea.getFont().deriveFont(Font.BOLD, 14f));

        add(new JScrollPane(logTextArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER), BorderLayout.SOUTH);
        JTextAreaOutputStream out = new JTextAreaOutputStream(logTextArea);
        System.setOut(new PrintStream(out));

//        add(mainWindowPanel, BorderLayout.CENTER);
    }
}
